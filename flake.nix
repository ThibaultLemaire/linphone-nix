{
  description = "Experimental Nix flake orchestration for the Linphone app & libraries";

  inputs = {
    bc-sofia-src = {
      url = git+https://gitlab.linphone.org/BC/public/external/sofia-sip.git;
      flake = false;
    };
  };

  outputs = { self, nixpkgs, bc-sofia-src, ... }: {

    nixUtils = {
      overrideSource = src: {
        inherit src;
        version = "${builtins.substring 0 8 (src.lastModifiedDate or src.lastModified or "19700101")}.${src.shortRev or "dirty"}";
      };
    };

    devShells.x86_64-linux.default = with nixpkgs.legacyPackages.x86_64-linux;
      mkShell {
        buildInputs = [
          nixpkgs-fmt
        ];
      };


    overlays.default = final: prev: with self.nixUtils; {
      bc-sofia-sip = prev.sofia_sip.overrideAttrs (attrs: (overrideSource bc-sofia-src) // {
        name = "bc-sofia-sip";
      });
    };

    packages.x86_64-linux = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
        self.overlays.default
      ];
    };

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.hello;

  };
}
